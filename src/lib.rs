#![allow(non_snake_case)]
mod api;

use api::{Input, Preset};
use dioxus::hooks::{use_future, use_state};
use dioxus::prelude::{
    dioxus_elements, format_args_f, rsx, Element, LazyNodes, NodeFactory,
    Scope, VNode,
};
use tracing::{debug, instrument};

use crate::api::{request, Success};

const DEFAULT_CONTENT: &str =
    "本 웹 앱은 러스트 言語와 웹 어셈블리 技術을 利用해 開發된 實驗的인 <선비> 프론트엔드입니다.";

#[instrument(skip(cx))]
pub fn App(cx: Scope) -> Element {
    let input = use_state(&cx, || Input {
        content: DEFAULT_CONTENT.to_string(),
        content_type: "text/plain".to_string(),
        preset: Preset::KoKR,
    });
    let last_input = use_state(&cx, || (*input.current()).to_owned());
    let nonce = use_state(&cx, || 0);
    let result = {
        let input = input.to_owned();
        let last_input = last_input.to_owned();
        let nonce = nonce.to_owned();
        use_future(&cx, (), |()| async move {
            let next_nonce = *nonce.current() + 1;
            nonce.modify(|_| next_nonce);
            let result = request(input.current().as_ref()).await;
            last_input.modify(|_| (*input.current()).to_owned());
            (result, next_nonce)
        })
    };
    let (result_content, loading) = match result.value() {
        Some((Ok(Success { content, .. }), last_nonce)) => {
            (content.to_owned(), *last_nonce != *nonce.current())
        }
        _ => ("Loading\u{2026}".to_string(), true),
    };
    debug!("input: {:?}", input);
    cx.render(rsx! {
        main {
            class: "container",
            h1 { "Seonbi" }
            div {
                class: "grid",
                div {
                    textarea {
                        value: "{input.content}",
                        cols: "80",
                        rows: "20",
                        oninput: |e| {
                            debug!("on_change: {}", e.value);
                            input.with_mut(|i| {
                                (*i).content = e.value.to_owned();
                            });
                        },
                    }
                }
                article {
                    aria_busy: "{loading}",
                    "{result_content}"
                }
            }
            div {
                button {
                    aria_busy: "{loading}",
                    disabled: "{loading}",
                    onclick: move |_| result.restart(),
                    "Transform"
                }
            }
        }
    })
}
